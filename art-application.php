<html>
	<head>
		<meta charset="UTF-8">
		<title>Mecenas - The art application</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<link href="css/bootstrap-theme.min.css" rel="stylesheet" />
		<link href="css/art-application.css" rel="stylesheet" />

		<script src='js/jquery-3.1.1.min.js'></script>
		<script src='js/bootstrap.min.js'></script>
		<script src='js/chart-utils.js'></script>
		<script src='js/Chart.bundle.min.js'></script>
	</head>

	<body>	
		<div style="margin-top: 50px;margin-top: 50px;width: 43%;margin-right: auto;margin-left: auto;">
			<center style="position: relative">
				<span class="keywords">Banksy<span class="glyphicon glyphicon-chevron-up trend-arrow" style="color: green" aria-hidden="true"></span></span>
				<span class="keywords">Tinguely<span class="glyphicon glyphicon-chevron-up trend-arrow" style="color: green" aria-hidden="true"></span></span>
				<span class="keywords">Raul de Nieves<span class="glyphicon glyphicon-chevron-up trend-arrow" style="color: green" aria-hidden="true"></span></span>
				<span class="keywords">Donna Huanca<span class="glyphicon glyphicon-chevron-up trend-arrow" style="color: green" aria-hidden="true"></span></span>
				<span class="keywords">Anish Kapoor<span class="glyphicon glyphicon-chevron-down trend-arrow" style="color: red" aria-hidden="true"></span></span>
				<span class="keywords">Jordan Wolfson<span class="glyphicon glyphicon-chevron-up trend-arrow" style="color: green" aria-hidden="true"></span></span>
				<span class="keywords">Terry Rodgers<span class="glyphicon glyphicon-chevron-up trend-arrow" style="color: green" aria-hidden="true"></span></span>
				<span class="keywords">Johan Grimonprez<span class="glyphicon glyphicon-chevron-up trend-arrow" style="color: green" aria-hidden="true"></span></span>
			</center>
		</div>
		<center>
			<h4 id="helper" style="font-family: helvetica; font-weight: 200">Pick an artist to see trend results</h4>
		</center>

		<div class="row" class="my-chart" style="margin-top: 70px;padding: 0 31px; position: relative">
			<object id="loader" data="images/gears.svg" type="image/svg+xml" style="display: none">
			</object>
			<div id="charts-container">
				<div class="col-md-6">
		        	<canvas id="mention-canvas"></canvas>
		    	</div>
				<div class="col-md-6">
		        	<canvas id="sentiment-canvas"></canvas>
		    	</div>
	    	</div>
		</div>
		<div class="row" id="quarter-legend" style="display: none; margin-top: 28px">
			<center>
				<div class="col-xs-3">
					<h5>1st quarter</h5>
					<p id="first-quarter"></p>
				</div>
				<div class="col-xs-3">
					<h5>2nd quarter</h5>
					<p id="second-quarter"></p>
				</div>
				<div class="col-xs-3">
					<h5>3rd quarter</h5>
					<p id="third-quarter"></p>
				</div>
				<div class="col-xs-3">
					<h5>4th quarter</h5>
					<p id="fourth-quarter"></p>
				</div>
			</center>
		</div>

    	<script type="text/javascript"> 
	 		var prepareGraphics = function(report){
		        var color = Chart.helpers.color;
		       
		        var mentionHorizontalBar = {
		            labels: [ 
		       			"1st quarter", "2nd quarter", "3rd quarter", "4th quarter"     
		            	],
		            datasets: [{
		                label: 'Mentions number',
		                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
		                borderColor: window.chartColors.red,
		                borderWidth: 1,
		                data: [
		                report.firstTermMentionNumber, 
		                report.secondTermMentionNumber, 
		                report.thirdTermMentionNumber, 
		                report.fourthTermMentionNumber
		                ]
		            }]

		        };
				var sentimentHorizontalBar = {
		            labels: [ 
		            "", "", "", ""
		            	],
		            datasets: [{
		                label: 'Sentiment value',
		                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
		                borderColor: window.chartColors.red,
		                borderWidth: 1,
		                 data: [
		                report.firstTermSentimentNumber, 
		                report.secondTermSentimentNumber, 
		                report.thirdTermSentimentNumber, 
		                report.fourthTermSentimentNumber
		                ]
		            }]

		        };

	            var ctx = document.getElementById("mention-canvas").getContext("2d");
	            window.myHorizontalBar = new Chart(ctx, {
	                type: 'horizontalBar',
	                data: mentionHorizontalBar,
	                options: {
	                	legend: {
					        display: false
					    },
	                    // Elements options apply to all of the options unless overridden in a dataset
	                    // In this case, we are setting the border of each horizontal bar to be 2px wide
	                    elements: {
	                        rectangle: {
	                            borderWidth: 2,
	                        }
	                    },
	                    responsive: true,
	                    // legend: {
	                    //     position: 'right',
	                    // },
	                    title: {
	                        display: true,
	                        text: "Number of articles where it's mentioned"
	                    }
	                }
	            });
					var ctx2 = document.getElementById("sentiment-canvas").getContext("2d");
	            window.myHorizontalBar = new Chart(ctx2, {
	                type: 'horizontalBar',
	                data: sentimentHorizontalBar,
	                options: {
	                	legend: {
					        display: false
					    },
	                    // Elements options apply to all of the options unless overridden in a dataset
	                    // In this case, we are setting the border of each horizontal bar to be 2px wide
	                    elements: {
	                        rectangle: {
	                            borderWidth: 2,
	                        }
	                    },
	                    responsive: true,
	                    // legend: {
	                    //     position: 'right',
	                    // },
	                    title: {
	                        display: true,
	                        text: 'Sentiment of artist per quarter'
	                    }
	                }
	            });


		        var colorNames = Object.keys(window.chartColors);
			}
		        //     window.myHorizontalBar.update();



			// var report = '{"firstTermMentionNumber":1,"secondTermMentionNumber":2,"thirdTermMentionNumber":0,"fourthTermMentionNumber":3,"firstTermSentimentNumber":0.0,"secondTermSentimentNumber":-0.1556365,"thirdTermSentimentNumber":0.0,"fourthTermSentimentNumber":0.2144596,"shortTermMentionGrow":200.0,"midTermMentionGrow":0.0,"longMidTermMentionGrow":66.66666666666667,"shortTermSentimentGrow":513.3855490196707,"midTermSentimentGrow":237.79518300655695,"longMidTermSentimentGrow":"Infinity","dateMargin":{"longTermDate":"27-05-2015","longMidTermDate":"16-10-2015","midTermDate":"06-03-2016","shortTermDate":"26-07-2016"}}';
			$(".keywords").click(function(){
				$("#helper").fadeOut();
				var artistName = $(this).text();
				console.log("clicked");
				$("#quarter-legend").fadeOut();
				$("#charts-container").fadeOut(function(){
					$('#mention-canvas').remove(); // this is my <canvas> element
					$('#sentiment-canvas').remove(); // this is my <canvas> element
  					$('#charts-container').append('<div class="col-md-6"><canvas id="mention-canvas"></canvas></div><div class="col-md-6"><canvas id="sentiment-canvas"></canvas></div>');
					$("#loader").fadeIn();
					console.log("Artist name: " + artistName);
					
					getReport(artistName, function(data){
						console.log("Got data: " + JSON.stringify(data));
						var waitTime = 0;
						if(gotReports.indexOf(artistName) == -1){
							waitTime = Math.ceil(Math.random()*2*1000);
							console.log("setting random time: " + waitTime);								
						}
						setTimeout(function(){
							$("#loader").fadeOut(function(){
								$("#first-quarter").text(data.dateMargin.longTermDate + " to " + data.dateMargin.longMidTermDate);
								$("#second-quarter").text(data.dateMargin.longMidTermDate + " to " + data.dateMargin.midTermDate);
								$("#third-quarter").text(data.dateMargin.midTermDate + " to " + data.dateMargin.shortTermDate);
								$("#fourth-quarter").text(data.dateMargin.shortTermDate + " to today");
								$("#quarter-legend").fadeIn();
								$("#charts-container").fadeIn();
								prepareGraphics(data);
							});
						}, waitTime);
						
					});
					
				});
			// prepareGraphics(JSON.parse(report));
			});
			var getReport = function(artistName, callback) {
					if(reports[artistName]){
						if(gotReports.indexOf(artistName) != -1)
							gotReports.push(artistName);
						console.log("in memory");						
						callback(JSON.parse(reports[artistName]));
					}
					else{
						var myquery = {
					    	"keyword": artistName
						};
						var queryString = $.param(myquery);
						$.getJSON("getArtistTrendReport.php?" + queryString, function(data){
							callback(data);	
						});
					}
			}
			var reports = {
				"Tinguely": '{"firstTermMentionNumber":3,"secondTermMentionNumber":9,"thirdTermMentionNumber":9,"fourthTermMentionNumber":39,"firstTermSentimentNumber":0.0,"secondTermSentimentNumber":-0.13317954999999998,"thirdTermSentimentNumber":0.0586392,"fourthTermSentimentNumber":0.024967599999999996,"shortTermMentionGrow":457.1428571428571,"midTermMentionGrow":300.0,"longMidTermMentionGrow":533.3333333333333,"shortTermSentimentGrow":200.48624671067415,"midTermSentimentGrow":162.77750600598966,"longMidTermSentimentGrow":"-Infinity","dateMargin":{"longTermDate":"31-03-2006","longMidTermDate":"03-12-2008","midTermDate":"08-08-2011","shortTermDate":"12-04-2014"}}',
				"Banksy": '{"firstTermMentionNumber":1,"secondTermMentionNumber":31,"thirdTermMentionNumber":27,"fourthTermMentionNumber":143,"firstTermSentimentNumber":0.0,"secondTermSentimentNumber":0.15560785,"thirdTermSentimentNumber":-0.09572582000000002,"fourthTermSentimentNumber":0.17740845358974366,"shortTermMentionGrow":627.1186440677966,"midTermMentionGrow":431.25,"longMidTermMentionGrow":6600.0,"shortTermSentimentGrow":788.7897767815006,"midTermSentimentGrow":-47.50738244263149,"longMidTermSentimentGrow":"Infinity","dateMargin":{"longTermDate":"24-07-2004","longMidTermDate":"29-08-2007","midTermDate":"04-10-2010","shortTermDate":"09-11-2013"}}',
				"Raul de Nieves": '{"firstTermMentionNumber":1,"secondTermMentionNumber":0,"thirdTermMentionNumber":2,"fourthTermMentionNumber":9,"firstTermSentimentNumber":0.0,"secondTermSentimentNumber":0.0,"thirdTermSentimentNumber":0.10763940000000001,"fourthTermSentimentNumber":0.0,"shortTermMentionGrow":800.0,"midTermMentionGrow":1000.0,"longMidTermMentionGrow":266.66666666666663,"shortTermSentimentGrow":-100.0,"midTermSentimentGrow":"Infinity","longMidTermSentimentGrow":"Infinity","dateMargin":{"longTermDate":"24-10-2013","longMidTermDate":"07-08-2014","midTermDate":"21-05-2015","shortTermDate":"03-03-2016"}}',
				"Donna Huanca": '{"firstTermMentionNumber":1,"secondTermMentionNumber":2,"thirdTermMentionNumber":0,"fourthTermMentionNumber":3,"firstTermSentimentNumber":0.0,"secondTermSentimentNumber":-0.1556365,"thirdTermSentimentNumber":0.0,"fourthTermSentimentNumber":0.2144596,"shortTermMentionGrow":200.0,"midTermMentionGrow":0.0,"longMidTermMentionGrow":66.66666666666667,"shortTermSentimentGrow":513.3855490196707,"midTermSentimentGrow":237.79518300655695,"longMidTermSentimentGrow":"Infinity","dateMargin":{"longTermDate":"27-05-2015","longMidTermDate":"16-10-2015","midTermDate":"06-03-2016","shortTermDate":"26-07-2016"}}',
				"Terry Rodgers": '{"firstTermMentionNumber":2,"secondTermMentionNumber":0,"thirdTermMentionNumber":0,"fourthTermMentionNumber":4,"firstTermSentimentNumber":0.1728392,"secondTermSentimentNumber":0.0,"thirdTermSentimentNumber":0.0,"fourthTermSentimentNumber":0.0,"shortTermMentionGrow":500.0000000000001,"midTermMentionGrow":100.0,"longMidTermMentionGrow":-33.333333333333336,"shortTermSentimentGrow":-100.0,"midTermSentimentGrow":-100.0,"longMidTermSentimentGrow":-100.0,"dateMargin":{"longTermDate":"13-10-2001","longMidTermDate":"29-07-2005","midTermDate":"15-05-2009","shortTermDate":"01-03-2013"}}',
				"Johan Grimonprez": '{"firstTermMentionNumber":2,"secondTermMentionNumber":1,"thirdTermMentionNumber":0,"fourthTermMentionNumber":3,"firstTermSentimentNumber":0.0,"secondTermSentimentNumber":0.0,"thirdTermSentimentNumber":0.0,"fourthTermSentimentNumber":0.0,"shortTermMentionGrow":200.0,"midTermMentionGrow":0.0,"longMidTermMentionGrow":-33.333333333333336,"shortTermSentimentGrow":"NaN","midTermSentimentGrow":"NaN","longMidTermSentimentGrow":"NaN","dateMargin":{"longTermDate":"27-01-2008","longMidTermDate":"17-04-2010","midTermDate":"06-07-2012","shortTermDate":"26-09-2014"}}',
				"Anish Kapoor": '{"firstTermMentionNumber":15,"secondTermMentionNumber":11,"thirdTermMentionNumber":8,"fourthTermMentionNumber":132,"firstTermSentimentNumber":-0.004673799999999999,"secondTermSentimentNumber":-0.015680114285714284,"thirdTermSentimentNumber":0.02720373333333333,"fourthTermSentimentNumber":-0.015481245901639344,"shortTermMentionGrow":1064.7058823529412,"midTermMentionGrow":438.4615384615385,"longMidTermMentionGrow":235.55555555555557,"shortTermSentimentGrow":-778.0286804957509,"midTermSentimentGrow":157.59328287985176,"longMidTermSentimentGrow":71.7743816307908,"dateMargin":{"longTermDate":"17-01-2006","longMidTermDate":"09-10-2008","midTermDate":"02-07-2011","shortTermDate":"25-03-2014"}}',
				"Jordan Wolfson": '{"firstTermMentionNumber":1,"secondTermMentionNumber":2,"thirdTermMentionNumber":0,"fourthTermMentionNumber":2,"firstTermSentimentNumber":0.0,"secondTermSentimentNumber":-0.1601476,"thirdTermSentimentNumber":0.0,"fourthTermSentimentNumber":0.0,"shortTermMentionGrow":100.0,"midTermMentionGrow":-33.33333333333333,"longMidTermMentionGrow":33.33333333333333,"shortTermSentimentGrow":100.0,"midTermSentimentGrow":100.0,"longMidTermSentimentGrow":"-Infinity","dateMargin":{"longTermDate":"14-10-2014","longMidTermDate":"30-04-2015","midTermDate":"14-11-2015","shortTermDate":"31-05-2016"}}'
			};
			var gotReports = [];
    	</script>
	</body>
</html>
