var s3Address = "https://s3-eu-west-1.amazonaws.com/mecenas/"

var trendExplanation = "Average sentiment growth for the short term";

var userLikes = [];
var venuesBasicInfo = [];
var venuesArray = null;
var venueIdToTrend = {};

var mixer;

var init = function(){
	getUserLikes(function(){
		getAllVenues(function(venues){
			this.venuesArray = venues;
			console.log(map);
			console.log(markersSet);
			if(map && !markersSet){
				console.log("Setting markers");
				setMarkers(venuesArray);
			}
				
			venues.forEach(function(item, index){
				addVenueDiv(item);
			});
			venues.forEach(function(item, index){
				doVenueWork(item);
			});

		});
	});
}

var getAllVenues = function(callback){
			console.log("calling getallvenues");

	$.getJSON("getAllVenues.php", function(data){
		console.log("called getallvenues");
		venuesBasicInfo = data;
		callback(data);
	});
}

var doIt = true;
var doVenueWork = function(item) {
	console.log(item.locationId);
	if(item != undefined && item.locationId != undefined && item.locationId != ""){
		if(doIt){
			// doIt = false;
			console.log("Doing venue work");
			console.log("Address: " + "getVenueTrendReport.php?id=" + item.locationId);
			$.ajax({
			  url: "getVenueTrendReport.php?id=" + item.locationId,
			  timeout: 0
			})
			  .done(function( data ) {
			    if(data != undefined) {	
			    	console.log(data);
			    	var venueDiv = $("#venue-" + item.locationId);
					venueDiv.find(".loading-image").eq(0).fadeOut();
					setTrendValue(item.locationId, JSON.parse(data), venueDiv);
				}
			  });
		}
			
	}
}


var addVenueDiv = function(item){
	var newVenue = $('<div id="venue-' + item.locationId + '"></div>');
	newVenue.addClass('mix');
	newVenue.addClass(item.category);
	if(userLikes.indexOf(item.locationId) != -1)
		newVenue.addClass("favorites");
	newVenue.attr("data-order", -20);

	var venueTitle = $('<h4 class="venue-title">' + item.locationName + '</h4>');
	newVenue.append(venueTitle);
	// newVenue.css("display", "none");
	var infoOverlay = $('<div class="info-overlay"></div>');
	var infoOverlayChild = $('<div style="position: relative; width; 100%; height: 100%"></div>');
	infoOverlay.append(infoOverlayChild);
	newVenue.append(infoOverlay);
	var like = $("<button></button>");

	var likeImg = $('<span></span>');
	like.addClass("like-button");
	like.append(likeImg);
	likeImg.addClass("glyphicon glyphicon-heart");
	infoOverlayChild.append(like);

	if(userLikes.indexOf(item.locationId) != -1)
		likeImg.addClass("like-button-active");
	
	newVenue.append(like);
	
	var trendValue = $('<div class="trend-value"></div>');
	var loadingImage = $('<img src="images/preloader.gif" class="loading-image"></img>');
	trendValue.append(loadingImage);

	infoOverlayChild.append(trendValue);

	var img = $('<img>');
	img.attr("src", s3Address + item.locationId + ".jpg");
	
	newVenue.append(img);
	
	$(".container").append(newVenue);
	// newVenue.fadeIn(2000);
	newVenue.on('mouseenter', function() {
		idToGraphics[item.locationId].marker.setIcon("http://maps.google.com/mapfiles/ms/icons/blue-dot.png");
		// idToGraphics[item.locationId].infoWindow.open(map, idToGraphics[item.locationId].marker);

	});
	newVenue.on('mouseleave', function(){
		idToGraphics[item.locationId].marker.setIcon("http://maps.google.com/mapfiles/ms/icons/red-dot.png");
		// idToGraphics[item.locationId].infoWindow.close(map, idToGraphics[item.locationId].marker);
	});

	like.on('click', function(){
		likeClicked = true;
		if(likeImg.hasClass("like-button-active"))
			disLikeVenue($(this), item.locationId);
		else
			likeVenue($(this), item.locationId);
		likeImg.toggleClass("like-button-active");
	});
	newVenue.on('click', function(){
		if(!likeClicked){
			venueClick(item);
		}
		else
			likeClicked = false;
	});
	mixer.forceRefresh();

	return newVenue;
}
var likeClicked = false;

var venueClick = function(item) {
	$("#black-box").fadeIn();
	$("#content-box").fadeIn();
	$("#close").text("x");

	$("#close").click(function() {
		$("#black-box").fadeOut();
		$("#content-box").fadeOut();
    });
    $("#black-box").click(function() {
		$("#black-box").fadeOut();
		$("#content-box").fadeOut();
    });

    $("#venue-image").attr("src", s3Address + item.locationId + ".jpg");
    $("#venue-title").text(item.locationName);
    $("#venue-description").text(item.description);
    var venueDiv = $("#venue-" + item.locationId).find(".trend-value").first();
    $("#venue-trend-value").empty();
    $("#venue-trend-value").append('<span style="float: left; margin-right: 3px; font-weight: bold">Trend value: </span>');
    $("#venue-trend-value").append(venueDiv.children().eq(1).clone());
    $("#venue-trend-value").append(venueDiv.children().eq(2).clone());

    $("#directions-container").click(function(){
    	getDirectionsFromPopup(item.locationId);
    });
}


var getDirectionsFromPopup = function(locationId) {
	    $('html, body').animate({
	        scrollTop: $("#map_canvas").offset().top
	    }, 1000);
	$("#black-box").fadeOut();
	$("#content-box").fadeOut();
	getDirections(locationId);
}

var getUserLikes = function(callback) {
	$.getJSON("getUserLikes.php", function(data){
		userLikes = data;
		callback();
	});
}
//Function receives the main venue div from jquery trigger
var likeVenue = function(heartSpan, venueId) {
	var venueDiv = heartSpan.parent();
    var params = {
        "id": venueId
    };
	$.get("userLikes.php?" + $.param(params), function(data){
		console.log("Data: " + data);
	});
	$("#venue-" + venueId).addClass("favorites");
	mixer.forceRefresh();
}
var disLikeVenue = function(heartSpan, venueId) {
	var venueDiv = heartSpan.parent();
    var params = {
        "id": venueId
    };
	$.get("removeUserLike.php?" + $.param(params), function(data){
		console.log("Data: " + data);
	});
	$("#venue-" + venueId).removeClass("favorites");
	mixer.forceRefresh();
}

$(document).ready(function(){
	mixer = mixitup('#Container');
	init();
});

var setTrendValue = function(locationId, data, venueDiv){
	var trendValueQuantity = 0;
	var averageTrendValue = 0;

	if(data.exhibitions){
		console.log("There are exhibitions");
		for(i = 0; i < data.exhibitions.length; i++){
			if(data.exhibitions[i].artists){
				console.log("There are artists");
				for(j = 0; j < data.exhibitions[i].artists.length; j++){
					if(data.exhibitions[i].artists[j].report && data.exhibitions[i].artists[j].report != ""){
						console.log("There are reports");
						var report = JSON.parse(data.exhibitions[i].artists[j].report);
						if(report.shortTermSentimentGrow){
							if(!isNaN(report.shortTermSentimentGrow)){
								trendValueQuantity ++;
								averageTrendValue += report.shortTermSentimentGrow;	
							}
						}
						
					}
				}	
			}
		}	
	}
	else
		console.log("There aren't any exhibitions");
	if(averageTrendValue != 0 && !isNaN(averageTrendValue)){
		averageTrendValue = ((averageTrendValue / trendValueQuantity) / 100);
		// averageTrendValue = Math.round(averageTrendValue * 100) / 100
		averageTrendValue = precise_round(averageTrendValue / 100, 2);
	}
	else
		averageTrendValue = 0;
	venueIdToTrend[locationId] = averageTrendValue;
	
	venueDiv.find(".trend-value").first().append('<div class="trend-value-number" data-toggle="tooltip" data-placement="right" title="' + trendExplanation + '">' + averageTrendValue + '</div>');
	if(averageTrendValue > 0)
		venueDiv.find(".trend-value").first().append('<span class="glyphicon glyphicon-chevron-up trend-arrow" style="color: green" aria-hidden="true"></span>');
	if(averageTrendValue < 0)
		venueDiv.find(".trend-value").first().append('<span class="glyphicon glyphicon-chevron-down trend-arrow" style="color: red" aria-hidden="true"></span>');

	$('[data-toggle="tooltip"]').tooltip();
	
	venueDiv.attr("data-order", averageTrendValue);
	mixer.forceRefresh();

}

function precise_round(num, decimals) {
   var t = Math.pow(10, decimals);   
   return (Math.round((num * t) + (decimals>0?1:0)*(Math.sign(num) * (10 / Math.pow(100, decimals)))) / t).toFixed(decimals);
}

