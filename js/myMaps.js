          // This sample uses the Place Autocomplete widget to allow the user to search
          // for and select a place. The sample then displays an info window containing
          // the place ID and other information about the place that the user has
          // selected.

          // This example requires the Places library. Include the libraries=places
          // parameter when you first load the API. For example:
          // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    // var locationName = "";
    // var locationId = "";

    var map;
    var markersSet = false;
    var idToGraphics = {};
    var userMarker;
	// var markers1 = [];
	// var gmarkers1 = [];

    function initMap() {
        map = new google.maps.Map(document.getElementById('map_canvas'), {
          center: {lat: 52.377965, lng: 4.9002023},
          zoom: 12
        });
        if(venuesArray != undefined){
            console.log("Setting markers in maps.js. Venues array: " + venuesArray)
            setMarkers();
        }
        addUserLocationMarker();
    }

    var addUserLocationMarker = function(){
        console.log("Setting user location");
        if (navigator.geolocation) {
            var myLatLng;
            var setMarker = function(){
                if(!userMarker)
                    userMarker = new google.maps.Marker({
                        position: myLatLng,
                        map: map,
                        icon: 'images/locationIcon.png'
                    });
                else
                    userMarker.position = myLatLng;
            }
            if (location.protocol == 'https:')
                navigator.geolocation.getCurrentPosition(function(position){
                    myLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    setMarker();
                });
            else{
                myLatLng = new google.maps.LatLng(52.333809, 4.865602);
                setMarker();
            }
        } else {
            // x.innerHTML = "Geolocation is not supported by this browser.";
        }
        // checkLocation();
    }

    var checkLocation = function() {
        setTimeout(function(){
            addUserLocationMarker();
            checkLocation();
        }, 4000);
    }
	
    var setMarkers = function(){
        markersSet = true;
        var infoWindows = [];
        var bounds = new google.maps.LatLngBounds();

        venuesArray.forEach(function(venue, index){
            var locationId = venue.locationId;
            var infowindow = new google.maps.InfoWindow();
            infoWindows.push(infowindow);

            var marker = new google.maps.Marker({
              "map": map,
              "icon": "http://maps.google.com/mapfiles/ms/icons/red-dot.png"
            });
            idToGraphics[locationId] = {};
            idToGraphics[locationId].marker = marker;
            idToGraphics[locationId].infoWindow = infowindow;
            marker.addListener('click', function() {
                infoWindows.forEach(function(locationId, index){
                    locationId.close();
                });
                  infowindow.open(map, marker);
            });
            var request = {
                placeId: locationId
            };

            var service = new google.maps.places.PlacesService(map);
            
            service.getDetails(request, function callback(result, status) {
                if (status == google.maps.places.PlacesServiceStatus.OK) {
                    marker.setPlace({
                        placeId: result.place_id,
                        location: result.geometry.location
                    });
                    marker.setVisible(true);

                
                    bounds.extend(new google.maps.LatLng(result.geometry.location.lat(), result.geometry.location.lng()));

                    locationId = result.place_id;
                    locationName = result.name;

                    // infowindow.setContent('<div id="' + locationId + '" class="venue-marker"><strong><br>' + result.name + '</strong><br><button type="button" onclick="getDirections(this)">Directions</button>' +
                    infowindow.setContent('<div id="' + locationId + '" class="venue-marker"><h5 style="margin-top: 5px; margin-bottom: 2px; color: rgb(247, 97, 79)">' + result.name + '</h5><p style="margin-bottom: 4px">' +
                    result.formatted_address + '</p><button style="padding-left: 0;" onclick="seeMoreClick(\'' + locationId + '\')">See more</button>');
                    // infowindow.open(map, marker);

                    if(index == venuesArray.length - 1)
                        map.fitBounds(bounds);
                }
            });
            
        });
    }

    var check;

    var seeMoreClick = function(id){
        console.log("Clicked on see more");
        $("#venue-" + id).click();
    }

    var directionsService;
    var directionsDisplay;

    var getDirections = function(id){
        console.log("Directions to: " + id);
        if(!directionsDisplay)
            directionsDisplay = new google.maps.DirectionsRenderer();// also, constructor can get "DirectionsRendererOptions" object
        directionsDisplay.setMap(map); // map should be already initialized.

        var request = {
            origin : userMarker.getPosition(),
            destination : new google.maps.LatLng(idToGraphics[id].marker.place.location.lat(), idToGraphics[id].marker.place.location.lng()),
            travelMode : google.maps.TravelMode.WALKING
        };
        directionsService = new google.maps.DirectionsService(); 
        directionsService.route(request, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
            }
        });


    }
	


	