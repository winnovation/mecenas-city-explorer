<?php 

	// $venueControlPanelAddress = "localhost:3002/";
	$venueControlPanelAddress = "https://venue-control-panel.eu-gb.mybluemix.net/";

	
	$returned_content = get_data($venueControlPanelAddress . "app/getUserLikes");
	echo $returned_content;

	function get_data($url) {
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}

 ?>