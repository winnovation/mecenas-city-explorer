<?php 

	// $venueControlPanelAddress = "localhost:3002/";
	$venueControlPanelAddress = "https://venue-control-panel.eu-gb.mybluemix.net/";

	$returned_content = get_data($venueControlPanelAddress . "app/userLikes/" . $_GET["id"]);
	echo $returned_content;

	function get_data($url) {
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}

 ?>