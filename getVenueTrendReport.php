<?php 

	$venueControlPanelAddress = "https://venue-control-panel.eu-gb.mybluemix.net/";
	//$venueControlPanelAddress = "http://localhost:3002/";
	
	$returned_content = get_data($venueControlPanelAddress . "app/getVenueTrendReport/" . $_GET["id"]);
	echo $returned_content;

	function get_data($url) {
		$venueControlPanelPort = 3002;
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// curl_setopt($ch, CURLOPT_PORT, 3002);

		$data = curl_exec($ch);
		if($data === false)
			{
			    echo 'Curl error: ' . curl_error($ch);
			}
		curl_close($ch);
		return $data;
	}

 ?>