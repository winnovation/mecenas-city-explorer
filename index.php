<html>
	<head>
		<meta charset="UTF-8">
		<title>Mecenas</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<link href="css/bootstrap-theme.min.css" rel="stylesheet" />
		<script src='js/jquery-3.1.1.min.js'></script>
		<script src='js/bootstrap.min.js'></script>
	</head>

	<body style="position: fixed; top: 0; right: 0; width: 100%; height: 100%">
		<div style="position: relative; width: 100%; height: 100%">
			<div id="content">
				<center>
					<h2 style="font-family: helvetica">Welcome to Mecenas</h2>
					<h4 style="margin-bottom: 38px">Choose an app to continue</h4>
					<div style="border: 1px solid #fb593a"></div>
					<div id="images-container" class="row">
						<div class="col-md-6" id="art-image-container">
							<a href="art-application.php"><img src="images/art-application.png" class="image"></a>
						</div>
						<div id="city-image-container" class="col-md-6">
							<a href="city-explorer.php"><img src="images/mecenas.png" class="image"></a>
						</div>
					</div>
				</center>
			</div>
		</div>
		
	</body>
	<style type="text/css">
		.image {
			height: 80px;
		}

		h2, h4 {
			 animation: translateDown 2s;
		}

		/*#images-container {
			 animation: translateUp 2s;
		}
*/
		#art-image-container {
			 animation: translateRight 2s;
		}
		#city-image-container {
			animation: translateLeft 2s;
		}
		@keyframes translateDown {
		    from {
		        transform:translateY(-1000px);
		    }
		    to {
		        transform: translateY(0);
		    }
		}
		@keyframes translateUp {
		    from {
		        transform:translateY(1000px);
		    }
		    to {
		        transform: translateY(0);
		    }
		}
		@keyframes translateLeft {
			    from {
			        transform:translateX(1000px);
			    }
			    to {
			        transform: translateX(0);
			    }
			}
		@keyframes translateRight {
			    from {
			        transform:translateX(-1000px);
			    }
			    to {
			        transform: translateX(0);
			    }
			}
		#content {
			position: absolute;
			top: 50%; 
			transform: translateY(-77%); 
			width: 100%
		}
		#images-container {
			margin-top: 73px; 
			padding: 0 80px
		}
		@media(max-width: 992px) {
			#city-image-container {
				margin-top: 33px;
			}
			#content {
				top: 0;
				margin-top: 50px;
				position: relative;
				transform: translateY(0);
			}
			#images-container {
				padding: 0;
			}
		}

	</style>
</html>
