<html>
	<head>
		<title>Mecenas - The art application</title>
		<link rel="shortcut icon" href="images/logo.png"/>
		<link href="styles.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<div class="container">
			<div class="header">
				<div class="logo">
					<a href="index.php"><img src="images/mecenas.png" alt="Mecenas logo" /></a>
				</div><!--logo ends-->
				<div class="navigation">
					<p>
						<a href="plan_your_visit.php"><b>Plan your visit</b></a>
						<a href="galleries.php"><b>Galleries</b></a>
						<a href="events.php"><b>Events</b></a>
						<a href="#"><img src="images/search.png" height="15" width="15"/></a>
						
						<a href="login.php"><img src="images/login.png" height="15" width="15"/> Login</a>
					</p>
				</div><!--navigation ends-->
			</div><!-- Header ends here-->
			
			<div class="maincontent">
				<!-- main content goes here-->
				
				<!-- Carousel -->
				<script src="carousel.js"></script>
				<div id="carousel">
					<figure id="spinner">
						<img src="images/resized/ams.jpg" alt>
						<img src="images/resized/night.jpg" alt>
						<img src="images/resized/famous-abstract.jpg" alt>
						<img src="iamges/resized/painting.jpg" alt>
						<img src="images/resized/rembrandt_nightwatch.jpg" alt>
						<img src="images/resized/starry-night.jpg" alt>
						<img src="images/resized/unveiled-richard-phillips.jpg" alt>
						<img src="images/resized/amsterdam.jpg" alt>
					</figure>
				</div>
				<span style="float:left" class="ss-icon" onclick="galleryspin('-')">&lt;</span>
				<span style="float:right" class="ss-icon" onclick="galleryspin('')">&gt;</span>
				<!-- Carousel ends here -->
				
				<!-- Plan your visit table -->
				<table cellspacing="50" border="0" cellpadding="0">
					<tr valign="top" align="left">
						<td width="300">
							<p><b>Red Stamp Art Gallery</b></p>
							<p>Open today, 4:30p.m.-8p.m.</p>
							<p>Rusland 22, 1012CL, Amsterdam</p>
							<br><br><br>
							<a href="#"><b>Plan your visit <span>&#8594;</span></b></a>
						</td>
						<td width="2" bgcolor="gray"><br></td>
						<td width="300" valign="top" align="left">
							<p><b>Renssen Art Gallery</b></p>
							<p>Open daily, 11a.m.-5:30p.m.</p>
							<p>Nieuwe Spiegelstraat 44, 1017DG, Amsterdam</p>
							<br><br>
							<a href="#"><b>Plan your visit <span>&#8594;</span></b></a>
						</td>
						<td width="2" bgcolor="#gray"><br></td>
						<td width="300" valign="top" align="left">
							<p><b>No Man's Art Gallery</b></p>
							<p>Open today, 11:30a.m.-6p.m.</p>
							<p>Nieuwe Herengracht 123, 1011SB, Amsterdam</p>
							<br><br>
							<a href="#"><b>Plan your visit <span>&#8594;</span></b></a>
						</td>
					</tr>
				</table>
				<!-- Plan your visit table ends here -->
				
				<br><br><br><br><br><br>
				
				<!-- Events table -->
				<h3>
					<a href="#"><b>Events</b></a>
				</h3>
				<table cellspacing="50" border="0" cellpadding="0">
					<tr valign="top" align="left">
						<td width="300">
							<a href="#"><img src="images/event1.jpg" height="130" width="130"/></a>
							<br><br>
							<p><b>Altération Aléatoire</b></p>
							<br>
							<p>03 July 2016 – 09 October 2016</p>
							<p>Solo exhibition by Bertrand Peyrot</p>
						</td>
						<td width="2" ><br></td>
						<td width="300" valign="top" align="left">
							<a href="#"><img src="images/event2.jpg" height="130" width="130"/></a>
							<br><br>
							<p><b>Masters of Photography</b></p>
							<br>
							<p>7 October 2016 – 8 January 2017</p>
							<p>The Rijksmuseum exhibits a previously unknown series of photographs by Martin Gerlach</p>
						</td>
						<td width="2" ><br></td>
						<td width="300" valign="top" align="left">
							<a href="#"><img src="images/event3.jpg" height="130" width="130"/></a>
							<br><br>
							<p><b>Frans Post. Animals in Brazil</b></p>
							<br>
							<p>10 October 2016 – 15 January 2017</p>
							<p>Shows how Frans Post encountered and immortalized a fascinating new world</p>
						</td>
					</tr>
				</table>
				<!-- Events table ends here -->
				
				<br><br><br><br><br><br>
				
				<!-- Partners table -->
				<h3>
					Partners
				</h3>
				<table cellspacing="50" border="0" cellpadding="0">
					<tr valign="top" align="left">
						<td width="300">
							<a href="http://www.ibm.com/nl-nl/?lnk=fcc"><img src="images/ibm.png" height="50" width="90"/></a>
						</td>
						<td width="2" ><br></td>
						<td width="300" valign="top" align="left">
							<a href="http://www.iamsterdam.com/en/"><img src="images/i-amsterdam.jpg" height="50" width="200"/></a>
						</td>
						<td width="2" ><br></td>
						<td width="300" valign="top" align="left">
							<a href="https://www.amsterdam.nl/"><img src="images/municipality.png" height="80" width="200"/></a>
						</td>
					</tr>
				</table>
				<!-- Partners table ends here -->
				
				<br><br>
				
				<!-- main content ends here-->
			</div>
		</div><!-- Container ends here-->
		
		<div class="footer">
				<div><div>
				<br>
				<a href="#"><b>About us</b></a>
				<a href="#"><b>Contact us</b></a>
				<a href="#"><b>Press</b></a>
				
				<p>&copy; 2016 Mecenas - The art application</p>
		</div>
	</body>
</html>
