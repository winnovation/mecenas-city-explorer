<html>
	<head>
		<meta charset="UTF-8">
		<title>Mecenas - The city explorer</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<link href="css/bootstrap-theme.min.css" rel="stylesheet" />
		<link href="css/styles2.css" rel="stylesheet" type="text/css" />
		<link href="css/mapStyle.css" rel="stylesheet" />
		<script src='js/jquery-3.1.1.min.js'></script>
		<script src='js/bootstrap.min.js'></script>
		<script src='js/mixitup.min.js'></script>
		<script src='js/main.js'></script>
		<script src='js/myMaps.js'></script>
		<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyBvUZVn1k-MkEnkOqc_MSWWVQ9TAF50Ugk&libraries=places&callback=initMap' async defer></script>
	</head>

	<body>
		<div id="page">
			<!-- Header goes here -->
			<!-- Menu button goes here -->
			<div id="menu" style="height: 10%; padding: 10px; border-bottom: 1px solid rgba(195, 168, 166, 0.45);">
				<a href="index.php"><img src="images/mecenas.png" id="logo"></a>
 			</div>
			<!-- Logo ends here -->
			<!-- Header ends here -->
			
			<!-- Main content goes here -->
			<div class="content" style="height: 90%">
				<div class="row" style="height: 100%">
					<!-- Filters go here -->
					<div id="venuesPanel" class="col-md-5" style="background:#f2f2f2; overflow-y: scroll;">
						<div class="controls">
							<div>
								<label>Filter:</label>
								<button class="filter" data-filter="all">All</button>
								<button class="filter" data-filter=".galleries">Galleries</button>
								<button class="filter" data-filter=".museums">Museums</button>
								<button class="filter" data-filter=".others">Others</button>
								<button class="filter" data-filter=".favorites" style="color: fb593a; font-weight: 400">My favorites</button>
							</div>
							<div>
								<label>Sort by trend value:</label>
								<button class="sort" data-sort="order:desc">Desc</button>
								<button class="sort" data-sort="order:asc">Asc</button>
								<button class="sort" data-sort="order:random">Random</button>
							</div>
						</div>
						

						<div id="Container" class="container" style="width:100%;">

						</div>
					</div>
					<!-- Filters end here -->
					
					<!-- Map goes here -->
					<div class="col-md-7" style="height: 100%; padding-left: 0">
						<div id="map_canvas"></div>
					</div>
					<!-- Map ends here -->

				</div>

			</div>
			<!-- Main content ends here -->
		</div>		
		<div id="black-box" style="background: black; opacity: 0.7; width: 100%; height: 100%; position: fixed; top: 0; display: none"></div>
		<div id="content-box">
			<button id="close" style="position: absolute; color: black; right: 0; top: 0"></button>
			<div class="row">
				<div class="col-md-6">
					<img id="venue-image" src="" style="width: 100%">
					<div id="directions-container" style="text-align: right; margin-top: 8px; cursor: pointer">
						<span id="directions-text" style="font-weight: bold">Get directions</span>
						<img id="navigate-image" style="height: 30px" src="images/navigate-outline.png">
					</div>
				</div>
				<div class="col-md-6">
					<h3 id="venue-title"></h3>
					<p id="venue-description"></p>
					<p id="venue-trend-value"></p>
				</div>
			</div>
		</div>
	</body>
</html>
