<?php 

	$venueControlPanelAddress = "http://ec2-54-218-117-214.us-west-2.compute.amazonaws.com:8080/trend?keyword=";
	//$venueControlPanelAddress = "http://localhost:3002/";
	
	// $data = array('keyword'=>$_GET['keyword']);
	// http_build_query($data);
	$data = urlencode($_GET['keyword']);

	$returned_content = get_data($venueControlPanelAddress . $data);
	echo $returned_content;
	// echo $venueControlPanelAddress . $data;

	function get_data($url) {
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// curl_setopt($ch, CURLOPT_PORT, 3002);

		$data = curl_exec($ch);
		if($data === false)
			{
			    echo 'Curl error: ' . curl_error($ch);
			}
		curl_close($ch);
		return $data;
	}

 ?>